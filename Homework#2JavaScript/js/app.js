/*
Використовуючи циклічні конструкції, пробіли (&nbsp;) та зірки (*) намалюйте:
Порожній 
- прямокутник 
Заповнений 
- Рівнобедрений трикутник 
- Трикутник прямокутний
- Ромб
*/
// Рівнобедрений трикутник
for (let i = 0; i < 10; i++) {
  for (let j = i + 1; j < 10; j++) {
    document.write("&nbsp;");
  }
  for (let y = 1 + i; y > 0; y--) {
    document.write("*");
  }
  document.write("<br>");
}
document.write("<hr>");

// Ромб
for (let i = 0; i < 9; i++) {
  for (let j = i + 1; j < 10; j++) {
    document.write("&nbsp;");
  }
  for (let y = 1 + i; y > 0; y--) {
    document.write("*");
  }
  document.write("<br>");
}
for (let i = 8; i >= 0; i--) {
  for (let j = i + 1; j < 10; j++) {
    document.write("&nbsp;");
  }
  for (let y = 1 + i; y > 0; y--) {
    document.write("*");
  }
  document.write("<br>");
}
document.write("<hr>");

// Прямокутник
for (let i = 0; i < 10; i++) {
  for (let j = 0; j < 10; j++) {
    if (i === 0 || i === 9) {
      document.write("*");
    } else {
      if (j === 0 || j === 9) {
        document.write(
          "*",
          "&nbsp;",
          "&nbsp;",
          "&nbsp;",
          "&nbsp;",
          "&nbsp;",
          "&nbsp;",
          "&nbsp;",
          "&nbsp;"
        );
      } else {
        document.write("&nbsp;");
      }
    }
  }
  document.write("<br>");
}
document.write("<hr>");

// Прямокутний трикутник
for (let i = 0; i < 15; i++) {
  for (let y = 1 + i; y > 0; y--) {
    document.write("*");
  }
  document.write("<br>");
}
