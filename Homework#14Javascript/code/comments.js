const commentsPerPage = 10;
let currentPage = 1;

function getComments() {
  fetch("https://jsonplaceholder.typicode.com/comments")
    .then((response) => response.json())
    .then((comments) => {
      const startIndex = (currentPage - 1) * commentsPerPage;
      const endIndex = startIndex + commentsPerPage;
      const currentComments = comments.slice(startIndex, endIndex);

      const commentsContainer = document.getElementById("comments");
      commentsContainer.innerHTML = "";

      currentComments.forEach((comment) => {
        const commentElement = document.createElement("div");
        commentElement.className = "each-comment";
        commentElement.innerHTML = `
        <h5>Id: ${comment.id}</h5>
        <h2>Name: ${comment.name}</h2>
        <p>Date: ${new Date().getFullYear()}-${new Date().getMonth() < 10? `0${new Date().getMonth() + 1}`: new Date().getMonth() + 1}-${new Date().getDate()} <p>
        <p>Email: <a href="mailto:${comment.email}">${comment.email}</a></p>
        <p>Commentary: ${comment.body}</p>
        `;
        commentsContainer.appendChild(commentElement);
      });

      const totalPages = comments.length / commentsPerPage;
      const paginationContainer = document.getElementById("pagination");
      paginationContainer.innerHTML = "";
      const maxButtons = 3;
      let startButton, endButton;
      if (totalPages <= maxButtons) {
        startButton = 1;
        endButton = totalPages;
      } else if (currentPage <= maxButtons / 2) {
        startButton = 1;
        endButton = maxButtons;
      } else if (currentPage > totalPages - maxButtons / 2) {
        startButton = totalPages - maxButtons + 1;
        endButton = totalPages;
      } else {
        startButton = currentPage - Math.floor(maxButtons / 2);
        endButton = currentPage + Math.floor(maxButtons / 2);
      }

      if (currentPage > 1) {
        const prevButton = document.createElement("button");
        prevButton.innerText = "Prev";
        prevButton.addEventListener("click", () => {
          currentPage--;
          getComments();
        });
        paginationContainer.appendChild(prevButton);
      }

      for (let i = startButton; i <= endButton; i++) {
        const pageButton = document.createElement("button")
        pageButton.innerText = i;
        if (i === currentPage) {
          pageButton.classList.add("active")
        }
        pageButton.addEventListener("click", () => {
          currentPage = i;
          getComments();
        });
        paginationContainer.appendChild(pageButton);
      }

      if (currentPage < totalPages) {
        const nextButton = document.createElement("button")
        nextButton.innerText = "Next"
        nextButton.addEventListener("click", () => {
          currentPage++
          getComments()
        })
        paginationContainer.appendChild(nextButton);
      }
    })
}

getComments()
