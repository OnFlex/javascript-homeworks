const cardContainer = document.getElementById("app");
function getServer(url, callback = () => {}) {
  const ajax = new XMLHttpRequest();
  ajax.open("get", url);
  ajax.send();
  ajax.addEventListener("readystatechange", () => {
    if (ajax.readyState === 4 && ajax.status >= 200 && ajax.status < 300) {
      callback(JSON.parse(ajax.response));
    } else if (ajax.readyState === 4) {
      throw new Error(
        `Error in server request: ${ajax.status} / ${ajax.statusText}`
      );
    }
  });
}
function showCard(data) {
  data.results.forEach(
    ({ name, gender, height, skin_color, birth_year, homeworld }, index) => {
      const card = `<div class="card"> 
        <div class="photo">
          <img src="https://starwars-visualguide.com/assets/img/characters/${index + 1}.jpg" alt="card-photo"> 
          <div>Name: ${name}</div>
          <div>Gender: ${gender}</div>
          <div>Height: ${height}</div>
          <div>Skin-Color: ${skin_color}</div>
          <div>Birth-year: ${birth_year}</div>
          <div>Homeworld: <span class="homeworld" data-url="${homeworld}"></span></div>
          <button class="save-button">Save</button>
        </div>
      </div>`;
      cardContainer.insertAdjacentHTML("afterbegin", card);
      getServer(homeworld, (data) => {
        const homeworldName = data.name
        const homeworldElement = document.querySelector(`[data-url="${homeworld}"]`)
        homeworldElement.innerText = homeworldName;
      });

      const saveButton = document.querySelector(".save-button");
      saveButton.addEventListener("click", () => {
        const cardData = {
          name,
          gender,
          height,
          skin_color,
          birth_year,
          homeworld,
        };
        const cards = JSON.parse(localStorage.getItem("cards")) || [];
        cards.push(cardData);
        localStorage.setItem("cards", JSON.stringify(cards))
        alert("You save card this card")
      });
    }
  );
}
getServer("https://swapi.dev/api/people",showCard)





