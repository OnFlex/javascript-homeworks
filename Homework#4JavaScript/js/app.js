// Напиши функцию map(fn, array), которая принимает на вход функцию и массив, и обрабатывает каждый элемент массива этой функцией, возвращая новый массив.
// Следующая функция возвращает true, если параметр age больше 18. В ином случае она задаёт вопрос confirm и возвращает его результат.
// 1	function checkAge(age) {
// 2	if (age > 18) {
// 3	return true;
// 4	} else {
// 5	return confirm('Родители разрешили?');
// 6	} }
// Перепишите функцию, используя оператор '?' или '||'
function map(fn, array) {
  let result = [];
  for (let i = 0; i < array.length; i++) {
    result.push(fn(array[i]));
  }
  return result;
}

  function addOne(x) {
  return x + 1;
}

let numbers = [1, 2, 3];
let numbersPlusOne = map(addOne, numbers);
console.log(numbersPlusOne);

function checkAge(age) {
  if (age > 18) {
    return true;
  } else {
    return confirm("Родители разрешили?");
  }
}

let age = prompt("Enter your age:");
let allowed = checkAge(age);

 function checkAge(age) {
   return age > 18 ? true : confirm("Родители разрешили?");
 }
 function checkAge(age) {
   return age > 18 || confirm("Родители разрешили?");
 }
