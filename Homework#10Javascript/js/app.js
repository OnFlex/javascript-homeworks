/* Задача N-1
Створіть програму секундомір.
* Секундомір матиме 3 кнопки "Старт, Стоп, Скидання"
* При натисканні на кнопку стоп фон секундоміра має бути червоним,
    старт - зелений, скидання - сірий
* Виведення лічильників у форматі ЧЧ:ММ:СС
* Реалізуйте Завдання використовуючи синтаксис ES6 та стрілочні функції*/
const timeCollection = document.querySelectorAll("span")
timeCollection[0].className = "hours"
timeCollection[1].className = "minutes";
timeCollection[2].className = "seconds";
const hoursElement = document.querySelector(".hours");
const minutesElement = document.querySelector(".minutes");
const secondsElement = document.querySelector(".seconds");
 
let seconds = 00,
minutes = 00,
hours = 00,
interval

const startTimer = () => {
    if(seconds <= 9) {
        secondsElement.innerText = "0" + seconds
    }
    if(seconds > 9) {
        secondsElement.innerText = seconds
    }
    seconds++;
    if(seconds > 59) {
        minutes++
        minutesElement.innerText = "0" + minutes
        seconds = 0
        secondsElement.innerText = "0" + seconds
    }
   
    if(minutes <=9 ) {
        minutesElement.innerText = "0" + minutes
    }
    if (minutes > 9) {
        minutesElement.innerText = minutes
    }
    if(minutes > 59) {
        hours++
        hoursElement.innerText = "0" + hours
        minutes = 0
        minutesElement.innerText = "0" + minutes
    }
    if (hours <= 9) {
      hoursElement.innerText = "0" + hours;
    }
    if (hours > 9) {
      hoursElement.innerText = hours;
    }
}

const buttons = document.querySelectorAll("button")

const startBtn = buttons[0];
startBtn.id = "start"
startBtn.addEventListener("click", () => {
    document.querySelector(".container-stopwatch").style.background = "green";
    document.querySelector(".container-stopwatch").style.transition ="1s ease-in";
    if ((interval === undefined)) {
      interval = setInterval(startTimer, 1000);
    }
}
);

const stopBtn = buttons[1];
stopBtn.id = "stop"
const stopTimer = () => {
  document.querySelector(".container-stopwatch").style.backgroundColor = "red";
  timerStarted = false;
  clearInterval(interval);
  interval = undefined;
};
stopBtn.addEventListener("click", () => stopTimer());

const resetBtn = buttons[2];
resetBtn.id = "reset"
const resetTimer = () => {
  document.querySelector(".container-stopwatch").style.backgroundColor = "gray";
  hoursElement.innerText,minutesElement.innerText,secondsElement.innerText = "00"
  minutes = 0
  hours = 0
  seconds = 0
  clearInterval(interval);
};
resetBtn.addEventListener("click", () => resetTimer());

/* Задача N-2
Використовуючи JS Створіть поле для введення телефону та кнопку збереження
Користувач повинен ввести номер телефону у форматі 000-000-00-00
Після того як користувач натискає кнопку зберегти перевірте правильність введення номера,
якщо номер правильний
зробіть зелене тло і використовуючи document.location переведіть користувача на сторінку
https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg
якщо буде помилка, відобразіть її в діві до input. */

const saveBtn = document.getElementById("saveBtn");
const phoneNumber = document.getElementById("phoneNumber");
const errorMessage = document.getElementById("errorMessage");

saveBtn.addEventListener("click", checkPhoneNumber);

function checkPhoneNumber() {
  const phoneNumberValue = phoneNumber.value;
  const phoneNumberPattern = /^\d{3}-\d{3}-\d{2}-\d{2}$/;

  if (phoneNumberPattern.test(phoneNumberValue)) {
    phoneNumber.style.backgroundColor = "green";
    document.location ="https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg";
  }
   else {
    errorMessage.innerHTML = "Invalid phone number";
    phoneNumber.style.backgroundColor = "red";
  }
}

/* Задача N-3
Створіть слайдер кожні 3 сек змінюватиме зображення
Зображення для відображення
https://new-science.ru/wp-content/uploads/2020/03/4848-4.jpg
https://universetoday.ru/wp-content/uploads/2018/10/Mercury.jpg
https://naukatv.ru/upload/files/shutterstock_418733752.jpg
https://cdn.iz.ru/sites/default/files/styles/900x506/public/news-2018-12/20180913_zaa_p138_057.jpg
https://nnst1.gismeteo.ru/images/2020/07/shutterstock_1450308851-640x360.jpg
 */
const images = [
  "https://new-science.ru/wp-content/uploads/2020/03/4848-4.jpg",
  "https://universetoday.ru/wp-content/uploads/2018/10/Mercury.jpg",
  "https://naukatv.ru/upload/files/shutterstock_418733752.jpg",
  "https://cdn.iz.ru/sites/default/files/styles/900x506/public/news-2018-12/20180913_zaa_p138_057.jpg",
  "https://nnst1.gismeteo.ru/images/2020/07/shutterstock_1450308851-640x360.jpg",
];

let currentIndex = 0;
let imageElement = document.getElementById("sliderImage");

const startSlider = () => {
  currentIndex++;
  if (currentIndex === images.length) {
    currentIndex = 0;
  }
  imageElement.src = images[currentIndex];
};
setInterval(startSlider, 3000);


