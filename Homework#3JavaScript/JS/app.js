//Задача 1
const array1 = ["a", "b", "c"];
const array2 = [1, 2, 3];
const array3 = [].concat(array1, array2);
console.log(array3);

//Задача 2
const arr1 = ["a", "b", "c"];
const arrWith123 = arr1.push(1, 2, 3)
console.log(arr1)

//Задача 3
const arrayNormal = [1, 2, 3];
const arrayReverse = arrayNormal.reverse();
console.log(arrayReverse);
 
//Задача 4 
const array123 = [1, 2, 3];
const arrayWith456 = array123.push(4, 5, 6);
console.log(array123);

//Задача 5 
const array123un = [1, 2, 3];
const array456Start = array123un.unshift(4, 5, 6);
console.log(array123un);

//Задача 6
const arrayWithStrings = ["js", "css", "jq"];
console.log(arrayWithStrings[0]);

//Задача 7 
const array12345sl = [1, 2, 3, 4, 5];
const arraySl = array12345sl.slice(0, 3);
console.log(arraySl);

//Задача 8
const array12345spl = [1, 2, 3, 4, 5];
array12345spl.splice(1,2);
console.log(array12345spl);

//Задача 9 
const arraySpl = [1, 2, 3, 4, 5];
arraySpl.splice(2, 0 , 10);
console.log(arraySpl);

//Задача 10
const arraySort = [3, 4, 1, 2, 7];
arraySort.sort();
console.log(arraySort);

//Задача 11
const arrayHello = ['Привіт,','світ','!'];
arrayHello.splice(1,1,"Мир")
let res = arrayHello.join(" ")
document.write(res);

//Задача 12
const arrayStrSpl =["Привіт, ", "світ", "!"];
arrayStrSpl.splice(0, 1, 'Поки,');
console.log(arrayStrSpl);

//Задача 13
const array1Way = [1, 2, 3, 4, 5]
const array2Way = new Array(1, 2, 3, 4, 5);
console.log(array2Way);
console.log(array2Way);

// Задача 14
var arr = {
  ru: ["блакитний", "червоний", "зелений"],
  en: ["blue", "red", "green"],
};
document.write('<p>'+ arr.en[0]);

//Задача 15
let arrayPlus = ["a", "b", "c", "d"];
arrayPlus.splice(1,0, "+")
arrayPlus.splice(4, 0, "+");
arrayPlus.splice(3,0, ",")
document.write("<p>" + arrayPlus.join(""));

//Задача 16 + 17

var userInfo = prompt("Укажите  количество  элементов в массиве")
let arrayUser = [];
for(let i = 0; i <= userInfo; i++){
    arrayUser.push(userInfo - i)
    if((userInfo - i) % 2 === 0){
        document.write("<p>" + i)
    }
    else {
        document.write("<span>" + i)
    }
}
arrayUser.reverse();
console.log(arrayUser);
document.write("<p>" + arrayUser);
console.log(userInfo);

//Задача 18
 var vegetables = ['Капуста', 'Ріпа', 'Редиска', 'Морковка']; 
 var str = vegetables.toString()          
                document.write("<p>" + str); 
                console.log(vegetables)
                console.log(str)