const btnDrawCircle = document.getElementById("drawCircleBtn");
btnDrawCircle.addEventListener("click", function () {
  let diameterInput = document.createElement("input");
  diameterInput.setAttribute("type", "number");
  diameterInput.setAttribute("placeholder", "Enter diameter");
  btnDrawCircle.parentNode.insertBefore(
    diameterInput,
    btnDrawCircle.nextSibling
  );
  let drawBtn = document.createElement("button");
  drawBtn.innerText = "Draw";
  drawBtn.style.marginLeft = "10px";
  btnDrawCircle.parentNode.insertBefore(drawBtn, diameterInput.nextSibling);
  drawBtn.addEventListener("click", function () {
    diameterInput.remove();
    drawBtn.remove();
    btnDrawCircle.style.display = "none";
    let diameter = diameterInput.value;
    let container = document.createElement("div");
    container.style.display = "grid";
    container.style.gridTemplateColumns = "repeat(10, 1fr)";
    btnDrawCircle.parentNode.insertBefore(container, btnDrawCircle.nextSibling);
    if (diameter < 1 || diameter > 150) {
      let errorMessage = document.createElement("div");
      errorMessage.innerText = "Please enter a diameter between 1 and 150";
      errorMessage.style.color = "red";
      errorMessage.style.marginTop = "10px";
      btnDrawCircle.parentNode.insertBefore(
        errorMessage,
        btnDrawCircle.nextSibling
      );
    } else {
      for (let i = 0; i < 100; i++) {
        let circle = document.createElement("div");
        circle.className = "circle";
        circle.style.width = diameter + "px";
        circle.style.height = diameter + "px";
        circle.style.borderRadius = "50%";
        circle.style.backgroundColor = getRandomColor();
        container.style.gap = "10px";
        container.appendChild(circle);
        container.style.marginLeft = "20px";
        container.addEventListener("click", function (event) {
          if (event.target.classList.contains("circle")) {
            event.target.style.display = "none";
          }
        });
      }
    }
  });
});

function getRandomColor() {
  let letters = "0123456789ABCDEF";
  let color = "#";
  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}
