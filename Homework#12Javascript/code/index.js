/*
1. 
Прив'яжіть усім інпутам наступну подію - втрата фокусу кожен інпут виводить своє value в параграф з id="test"
focus blur input

2.
Дано інпути. Зробіть так, щоб усі інпути втрати фокусу перевіряли свій вміст на 
правильну кількість символів. Скільки символів має бути в інпуті, зазначається 
в атрибуті data-length. 
Якщо вбито правильну кількість, то межа інпуту стає зеленою, якщо неправильна – червоною.
*/
const [...inputs] = document.querySelectorAll("input");
const outputP = document.getElementById("test");

inputs.forEach((input) => {
  const dataLength = input.getAttribute("data-length");

  input.addEventListener("blur", () => {
    outputP.innerText += input.value + ","});

  input.addEventListener("blur", () => {
    if (input.value.length === parseInt(dataLength)) {
      input.style.borderColor = "green"
    } else {
      input.style.borderColor = "red"
    }
  });
});

/* 3.
- При завантаженні сторінки показати користувачеві поле введення (`input`) з написом `Price`. Це поле буде служити для введення числових значень
- Поведінка поля має бути такою:
- При фокусі на полі введення – у нього має з'явитися рамка зеленого кольору. При втраті фокусу вона пропадає.
- Коли забрали фокус з поля - його значення зчитується, над полем створюється `span`, в якому має бути виведений текст: 
. 
Поруч із ним має бути кнопка з хрестиком (`X`). Значення всередині поля введення фарбується зеленим.
- При натисканні на `Х` - `span` з текстом та кнопка `X` повинні бути видалені.
- Якщо користувач ввів число менше 0 - при втраті фокусу підсвічувати поле введення червоною рамкою, 
під полем виводити фразу - `Please enter correct price`. `span` зі значенням при цьому не створюється.
*/
window.addEventListener("DOMContentLoaded", () => {
  const inputContainer = document.createElement("div");
  inputContainer.classList.add("input-container");

  const priceInput = document.createElement("input");
  priceInput.setAttribute("type", "number");
  priceInput.setAttribute("placeholder", "Price");

  inputContainer.appendChild(priceInput);
  outputP.after(inputContainer);

  priceInput.addEventListener("focus", () => {
    priceInput.style.borderColor = "green"
  });

  priceInput.addEventListener("blur", () => {
    priceInput.style.borderColor = "black"
    const priceValue = priceInput.value;

    if (priceValue !== "") {
      const span = document.createElement("span");
      const spanText = document.createTextNode(priceValue);
      span.appendChild(spanText);

      const closeButton = document.createElement("button");
      closeButton.innerHTML = "X";
      closeButton.addEventListener("click", () => {
        span.remove();
      });

      span.appendChild(closeButton);
      priceInput.insertAdjacentElement("beforebegin", span);
      priceInput.style.color = "green"

      if (priceValue < 0) {
        priceInput.style.borderColor = "red"
        const errorSpan = document.createElement("span");
        errorSpan.textContent = "Please enter correct price"
        errorSpan.style.color = "red"
        priceInput.insertAdjacentElement("afterend", errorSpan);
        span.remove();
      };
    };
  });
});
