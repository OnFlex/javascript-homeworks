
const h2InBody = document.createElement("h2")
h2InBody.innerText = "BODY"
document.body.appendChild(h2InBody)

const header = document.createElement("header");
header.id = "page-header";
document.body.appendChild(header);

const h2InHeader = document.createElement("h2");
h2InHeader.textContent = "HEADER";
header.appendChild(h2InHeader);

const nav = document.createElement("nav");
nav.id = "page-nav"
header.appendChild(nav);
nav.classList.add ("navigation")

const h2InNav = document.createElement("h2");
h2InNav.textContent = "NAV"
nav.appendChild(h2InNav)

const main = document.createElement("main");
document.body.appendChild(main);

const section = document.createElement("section");
main.insertAdjacentElement("afterbegin", section);
section.id = "section";

const h2InSection = document.createElement("h2");
h2InSection.textContent = "SECTION";
section.insertAdjacentElement("afterbegin",h2InSection)

const headerInSection = document.createElement("header");
section.insertAdjacentElement("beforeend",headerInSection);
headerInSection.id = "header-in-section";

const h2InHeaderSection = document.createElement("h2");
h2InHeaderSection.textContent = "HEADER";
headerInSection.appendChild(h2InHeaderSection);

const articleInSection = document.createElement("article")
articleInSection.id = "article-in-section"
section.insertAdjacentElement("beforeend",articleInSection)

const h2InArticleInSection = document.createElement("h2")
h2InArticleInSection.innerText = "ARTICLE";
articleInSection.appendChild(h2InArticleInSection)

const headerInArticle = document.createElement("header");
headerInArticle.id = "header-in-article"
articleInSection.appendChild(headerInArticle);

const h2InHeaderArticle = document.createElement("h2")
h2InHeaderArticle.innerText = "HEADER"
headerInArticle.appendChild(h2InHeaderArticle)

const pInArticle = document.createElement("p");
pInArticle.className = "p-in-article"
pInArticle.innerText = "P";
articleInSection.appendChild(pInArticle)

const divInArticle = document.createElement("div")
divInArticle.className = "flex"
articleInSection.appendChild(divInArticle)

const pInDiv = document.createElement("p")
pInDiv.innerText = "P"
pInDiv.className = "p-in-div"
divInArticle.appendChild(pInDiv);

const asideInDiv = document.createElement("aside")
asideInDiv.id = "aside-in-div"
divInArticle.appendChild(asideInDiv);

const h2InAside = document.createElement("h2");
h2InAside.innerText = "ASIDE";
asideInDiv.appendChild(h2InAside)

const footerInArticle = document.createElement("footer")
footerInArticle.id = "footer-in-article"
articleInSection.appendChild(footerInArticle);

const h2InFooter = document.createElement("h2");
h2InFooter.textContent ="FOOTER"
footerInArticle.appendChild(h2InFooter)

const section2 = document.createElement("section");
main.insertAdjacentElement("beforeend",section2)
section2.id = "section2"

const h2InSection2 = document.createElement("h2");
h2InSection2.innerText = "SECTION"
section2.insertAdjacentElement("afterbegin",h2InSection2)

const headerInSection2 = document.createElement("header")
headerInSection2.id = "header-in-section2"
section2.appendChild(headerInSection2)

const h2InHeaderSection2 = document.createElement("h2");
h2InHeaderSection2.innerText = "HEADER";
headerInSection2.appendChild(h2InHeaderSection2);

const navInSection2 = document.createElement("nav");
navInSection2.id = "nav-in-section2"
section2.appendChild(navInSection2);

const h2InNavSection2 = document.createElement("h2");
h2InNavSection2.innerText = "NAV";
navInSection2.appendChild(h2InNavSection2);

const article2InSection = document.createElement("article")
article2InSection.id = "article2-in-section"
section.appendChild(article2InSection);

const h2InArticle2 = document.createElement("h2");
h2InArticle2.innerText = "ARTICLE"
article2InSection.appendChild(h2InArticle2)

const headerInArticle2 = document.createElement("header");
headerInArticle2.id = "header-in-article2"
article2InSection.appendChild(headerInArticle2);

const h2InHeaderArticle2 = document.createElement("h2");
h2InHeaderArticle2.innerText = "HEADER";
headerInArticle2.appendChild(h2InHeaderArticle2);

const pInArticle2 = document.createElement("p");
pInArticle2.id = "p-in-artilce2"
pInArticle2.innerText = "P";
article2InSection.appendChild(pInArticle2)

const p2InArticle2 = document.createElement("p");
p2InArticle2.id = "p2-in-article2"
p2InArticle2.innerText = "P";
article2InSection.appendChild(p2InArticle2);

const footerInArticle2 = document.createElement("footer");
footerInArticle2.id = "footer-in-article2"
article2InSection.appendChild(footerInArticle2);

const h2InFooter2 = document.createElement("h2");
h2InFooter2.innerText = "FOOTER"
footerInArticle2.appendChild(h2InFooter2)

const footer = document.createElement("footer");
footer.id = "footer"
document.body.appendChild(footer);

const h2inFooterpage = document.createElement("h2")
h2inFooterpage.innerText = "FOOTER"
footer.appendChild(h2inFooterpage)


const elements = document.querySelectorAll("h2, p");

for (let i = 0; i < elements.length; i++) {
  elements[i].addEventListener("click", function () {
    let newText = prompt("Enter new text for element,gonna be change on your text:");
    if (newText) {
      elements[i].innerText = newText;
    }
  });
}


