// // Реалізуйте клас Worker (Працівник), який матиме такі властивості: name (ім'я), surname (прізвище),
// // rate (ставка за день роботи), days (кількість відпрацьованих днів).

// // Також клас повинен мати метод getSalary(), який виводитиме зарплату працівника.
// // Зарплата - це добуток (множення) ставки rate на кількість відпрацьованих днів days.

class Worker {
  constructor(name, surname, rate, days) {
    this.name = name;
    this.surname = surname;
    this.rate = rate;
    this.days = days;
  }

  getSalary() {
    return this.rate * this.days;
  }
}

let worker = new Worker("Alex", "Dawson", 200, 20);
console.log(worker.getSalary());

// // Реалізуйте клас MyString, який матиме такі методи: метод reverse(),
// // який параметром приймає рядок, а повертає її в перевернутому вигляді, метод ucFirst(),
// // який параметром приймає рядок, а повертає цей же рядок, зробивши його першу літеру великою
// // та метод ucWords, який приймає рядок та робить заголовною першу літеру кожного слова цього рядка.

class MyString {
  reverse(str) {
    return str.split("").reverse().join("");
  }
  ucFirst(str) {
    return str[0].toUpperCase() + str.slice(1);
  }
  ucWords(str) {
    let words = str.split(" ");
    for (let i = 0; i < words.length; i++) {
      words[i] = words[i][0].toUpperCase() + words[i].slice(1);
    }
    return words.join(" ");
  }
}

const myString = new MyString();
console.log(myString.reverse("Hello World!"));
console.log(myString.ucFirst("hello world!"));
console.log(myString.ucWords("hello world!"));

// Створіть клас Phone, який містить змінні number, model і weight.
// Створіть три екземпляри цього класу.
// Виведіть на консоль значення їх змінних.
// Додати в клас Phone методи: receiveCall, має один параметр - ім'я.
// Виводить на консоль повідомлення "Телефонує {name}".
// Метод getNumber повертає номер телефону. Викликати ці методи кожного з об'єктів.

class Phone {
  constructor(number, model, weight) {
    this.number = number;
    this.model = model;
    this.weight = weight;
  }

  receiveCall(name) {
    console.log(`Phone call coming from ${name}`);
  }

  getNumber() {
    return this.number;
  }
}
const phone1 = new Phone("050-456-7890", "Iphone 12", "280gram");
const phone2 = new Phone("098-765-4321", "Iphone 7", "235gram");
const phone3 = new Phone("099-432-3333", "Samsung Galaxy S20", "200gram");
console.log(phone1);
console.log(phone2);
console.log(phone3);

phone1.receiveCall("Kevin Foden");
console.log(phone1.getNumber());

phone2.receiveCall("Emerik Laporte");
console.log(phone2.getNumber());

phone3.receiveCall("Bobby Green");
console.log(phone3.getNumber());

// Створити клас Car , Engine та Driver.
// Клас Driver містить поля - ПІБ, стаж водіння.
// Клас Engine містить поля – потужність, виробник.
// Клас Car містить поля – марка автомобіля, клас автомобіля, вага, водій типу Driver,
// мотор типу Engine.
// Методи start(), stop(), turnRight(), turnLeft(),
// які виводять на друк: "Поїхали", "Зупиняємося", "Поворот праворуч" або "Поворот ліворуч".
// А також метод toString(), який виводить повну інформацію про автомобіль, її водія і двигуна.

class Engine {
  constructor(power, manufacturer) {
    this.power = power;
    this.manufacturer = manufacturer;
  }
}

class Driver {
  constructor(name, experience) {
    this.name = name;
    this.experience = experience;
  }
}

class Car {
  constructor(brand, type, weight, driver, engine) {
    this.brand = brand;
    this.type = type;
    this.weight = weight;
    this.driver = driver;
    this.engine = engine;
  }

  start() {
    console.log("Поїхали");
  }

  stop() {
    console.log("Зупиняємося");
  }

  turnRight() {
    console.log("Поворот праворуч");
  }

  turnLeft() {
    console.log("Поворот ліворуч");
  }
  //нормально ли то что классы не связаны и я обращаюсь this.driver или this.engine ?
  toString() {
    return `Автомобіль: ${this.brand} ${this.type}, вага: ${this.weight} kg, 
        водій: ${this.driver.name} (${this.driver.experience} років стажу), 
        двигун: ${this.engine.manufacturer}, потужність: ${this.engine.power} к.с.`;
  }
}
const myDriver = new Driver("Олена Олександрівна Бджілка", 5);
const myEngine = new Engine(200, "Mersedes");
const myCar = new Car("Mersedes", "Sedan", 1500, myDriver, myEngine);
myCar.start();
myCar.turnRight();
myCar.stop();
console.log(myCar.toString());

class Lorry extends Car {
  constructor(brand, type, weight, driver, engine, payload) {
    super(brand, type, weight, driver, engine);
    this.payload = payload;
  }

  toString() {
    return super.toString() + `, вантажопідйомність: ${this.payload} кг.`;
  }
}

class SportCar extends Car {
  constructor(brand, type, weight, driver, engine, topSpeed) {
    super(brand, type, weight, driver, engine);
    this.topSpeed = topSpeed;
  }

  toString() {
    return super.toString() + `, гранична швидкість: ${this.topSpeed} км/г.`;
  }
}
const myLorryDriver = new Driver("Mike", 8);
const myLorryEngine = new Engine(300, "Volvo");
const myLorry = new Lorry(
  "Volvo",
  "Lorry",
  5000,
  myLorryDriver,
  myLorryEngine,
  15000
);
console.log(myLorry.toString());

const mySportCarDriver = new Driver("Sam", 2);
const mySportCarEngine = new Engine(500, "Ferrari");
const mySportCar = new SportCar(
  "Ferrari",
  "Sport",
  1000,
  mySportCarDriver,
  mySportCarEngine,
  300
);
console.log(mySportCar.toString());

// Створити клас Animal та розширюючі його класи Dog, Cat, Horse.
// Клас Animal містить змінні food, location і методи makeNoise, eat, sleep.
// Метод makeNoise, наприклад, може виводити на консоль "Така тварина спить".
// Dog, Cat, Horse перевизначають методи makeNoise, eat.
// Додайте змінні до класів Dog, Cat, Horse, що характеризують лише цих тварин.
// Створіть клас Ветеринар, у якому визначте метод void treatAnimal(Animal animal).
//  Нехай цей метод роздруковує food і location тварини, що прийшла на прийом.
// У методі main створіть масив типу Animal, в який запишіть тварин всіх типів, що є у вас.
//  У циклі надсилайте їх на прийом до ветеринара.

class Animal {
  constructor(food, location) {
    this.food = food;
    this.location = location;
  }
  makeNoise() {
    console.log("This animal is making some noise");
  }
  eat() {
    console.log(`The animal is eating ${this.food}`);
  }
  sleep() {
    console.log(`The animal is sleeping at ${this.location}`);
  }
}
class Dog extends Animal {
  constructor(food, location, breed) {
    super(food, location);
    this.breed = breed;
  }
  makeNoise() {
    console.log(`гав гав`);
  }
  eat() {
    console.log(`Dog is eating  ${this.food}`);
  }
  sleep() {
    console.log(`Dog  sleeping at ${this.location}`);
  }
}
class Cat extends Animal {
  constructor(food, location, color) {
    super(food, location);
    this.color = color;
  }
  makeNoise() {
    console.log(`Мяу мяу`);
  }
  eat() {
    console.log(`Cat is eating ${this.food}`);
  }
  sleep() {
    console.log(`Cat sleeping at ${this.location}`);
  }
}
class Horse extends Animal {
  constructor(food, location, color) {
    super(food, location);
    this.color = color;
  }
  makeNoise() {
    console.log("Игогого");
  }
  eat() {
    console.log(`Horse eating ${this.food}`);
  }
  sleep() {
    console.log(`Horse at ${this.location}`);
  }
}
const dog = new Dog("meat", "home", "Doberman");
dog.makeNoise();
dog.eat();
console.log(dog.breed);

const cat = new Cat("fish", "home", "White");
cat.makeNoise();
cat.eat();
console.log(cat.color);

const horse = new Horse("carrot", "stable", "Black");
horse.makeNoise();
horse.eat();
console.log(horse.color);

class Veterinarian {
  treatAnimals(animal) {
    console.log(`Food: ${animal.food}`);
    console.log(`Location: ${animal.location}`);
  }
}
//вот тут хочу получить обьяснение вообще не пойму как тут правильно нужно сделать сделать метод
//main без слова function не получаеться
function main() {
  let myAnimals = [
    new Dog("meat", "home", "Doberman"),
    new Cat("fish", "apartment", "White"),
    new Horse("horse food", "stable", "Black"),
  ];
  let vet = new Veterinarian();

  myAnimals.forEach((animal) => {
    vet.treatAnimals(animal);
    console.log();
  });
}
main();

