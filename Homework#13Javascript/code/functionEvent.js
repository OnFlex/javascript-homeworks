import { userSlectTopping, showPriceAndToppings,random } from "./functions.js";
import {
  pizzaBasic,
  pizzaSelectUser,
  sizeRef,
  sauceRef,
  toppingRef,
  addedToppings,
  addedSauce,
  nameFieldRef,
  phoneFieldRef,
  emailFieldRef,
  banner
  //   addedSauce,
} from "./index.js";

export function clickInputSize(e) {
  if (e.target.tagName === "INPUT") {
    userSlectTopping(e.target.value);
  }
}

export function dragstartHandler(e) {
 ;

  e.dataTransfer.effectAllowed = "move";

  e.dataTransfer.setData("ElementId", this.id);
  e.dataTransfer.setData("ImgUrl", this.src);
}

export function dragendHandler(e) {
  this.style.border = "";
}

export function dragenterHandler(e) {
  e.preventDefault();
  
}

export function dragoverHandler(e) {
  e.preventDefault();

  return false;
}

export function dropHandler(e) {
  if (e.preventDefault) e.preventDefault();
  if (e.stopPropagation) e.stopPropagation();

  this.style.border = "";
  const id = e.dataTransfer.getData("ElementId");
  const imgUrl = e.dataTransfer.getData("ImgUrl");
  const img = document.createElement("img");

  if ("sauceClassicsauceBBQsauceRikotta".includes(id)) {
    img.className = "draggable";
    img.src = imgUrl;
    img.zIndex = 1;

    addedSauce.length = 0;

    addedSauce.push(img);

    if (this.children.length === 1) {
      this.append(...addedSauce);
    }

    if (this.children.length > 1) {
      this.replaceChildren(
        pizzaBasic.children[0],
        ...addedSauce,
        ...addedToppings
      );
    }
  }

  if ("moc1moc2moc3telyavetch1vetch2".includes(id)) {
    img.className = "draggable";
    img.src = imgUrl;
    img.zIndex = 5;

    addedToppings.push(img);

    if (this.children.length === 1) {
      this.append(img);
    }

    if (this.children.length > 1) {
      if (addedSauce.length === 0) {
        this.replaceChildren(pizzaBasic.children[0], ...addedToppings);

        userSlectTopping(id);
        return false;
      }

      this.replaceChildren(
        pizzaBasic.children[0],
        ...addedSauce,
        ...addedToppings
      );
    }
  }

  userSlectTopping(id);

  return false;
}

export function resetBtnHandler(e) {
  const basicChild = pizzaBasic.children[0];

  pizzaBasic.replaceChildren(basicChild);

  pizzaSelectUser.size = "";
  pizzaSelectUser.topping = [];
  pizzaSelectUser.sauce = "";
  pizzaSelectUser.price = 0;
  addedToppings.splice(0, addedToppings.length);
  addedSauce.splice(0, addedSauce.length);

  const [...sizeBtns] = sizeRef.children;
  

  sizeBtns.forEach((el) => (el.children[0].checked = false));

  sauceRef.innerHTML = "";
  toppingRef.innerHTML = "";

  showPriceAndToppings(pizzaSelectUser);
}

export function orderPizza(e) {
  const phoneRegex = /^\+380\d{9}$/;
  const validatePhone = phoneRegex.test(phoneFieldRef.value);

  const emailRegex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;
  const validateEmail = emailRegex.test(emailFieldRef.value);

  if (nameFieldRef.value.length === 0) {
    alert(`Перевірте корректність ім'я`);
    return;
  }

  if (!validatePhone) {
    alert(`Перевірте корректність телефону`);
    return;
  }

  if (!validateEmail) {
    alert(`Перевірте корректність електронної пошти`);
    return;
  }

  if (pizzaSelectUser.size.length === 0) {
    alert("Виберіть розмір піцци");
    return;
  }

  if (pizzaSelectUser.sauce.length === 0) {
    alert("Вибреріть соус");
    return;
  }

  if (pizzaSelectUser.topping.length === 0) {
    alert("Виберіть топінг");
    return;
  }
  window.location.href = window.location.href + "thank-you";
}

export function tryCatch () {
      banner.style.right = `${random(0, 90)}%`;
      banner.style.bottom = `${random(0, 90)}%`;
}