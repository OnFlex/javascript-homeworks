const req = async (url) => {
  document.querySelector(".box_loader").classList.add("show");
  const data = await fetch(url);
  return await data.json();
};
const reqPlanet = async (url, page = 1) => {
  document.querySelector(".box_loader").classList.add("show");
  const data = await fetch(`${url}?page=${page}`);
  const result = await data.json();
  const { results, next } = result;
  if (next) {
    const nextPage = next.split("?page=")[1];
    const nextResults = await reqPlanet(url, nextPage);
    return [...results, ...nextResults];
  }
  return results;
};

const nav = document.querySelector(".nav").addEventListener("click", (e) => {
  if (e.target.dataset.link === "nbu") {
    req(
      "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json"
    ).then((info) => {
      show(info);
    });
  } else if (e.target.dataset.link === "star") {
    reqPlanet("https://swapi.dev/api/planets/").then((info) => {
      show(info);
    });
  } else if (e.target.dataset.link === "todo") {
    req("https://jsonplaceholder.typicode.com/todos").then((info) => {
      show(info);
    });
  } else {
  }
});

function show(data = []) {
  if (!Array.isArray(data)) return;

  const tbody = document.querySelector("tbody");
  tbody.innerHTML = "";
  const newArr = data.map(
    (
      { txt, rate, exchangedate, title, completed, name, terrain, population },
      i
    ) => {
      return {
        id: i + 1,
        name: txt || title || name,
        info1: rate || completed || terrain,
        info2: exchangedate || population || "тут пусто",
      };
    }
  );

  newArr.forEach(({ name, id, info1, info2 }) => {
    tbody.insertAdjacentHTML(
      "beforeend",
      `
        <tr> 
        <td>${id}</td>
        <td>${name}</td>
        <td>${info1}</td>
        <td>${info2}</td>
        </tr>
        `
    );
  });

  document.querySelector(".box_loader").classList.remove("show");
}
