const wrapper = document.createElement("div");
wrapper.classList.add("wrapper");
document.body.insertAdjacentElement("afterbegin",wrapper);


const imgContainer = document.createElement("div")
imgContainer.classList.add("img-container")
wrapper.appendChild(imgContainer);


const imgRegForm = document.createElement("img");
imgRegForm.src = "./img/Figure.png";
imgRegForm.alt = "img-regform";
imgContainer.appendChild(imgRegForm)



const form = document.createElement("form");
form.classList.add("reg-form-container");

wrapper.appendChild(form);


const regFormPage = `
          <h1 class="reg-form-text">Registration form</h1>
          <label for="name">Name</label>
          <div class="reg-form-validator">
            <input type="text" id="name" name="name" />
            <span class="error-symbol-name" id="error-symbol-name"
              >&#10060;</span>
            <span class="done-symbol-name" id="done-symbol-name">&#128504;</span>
          </div>

          <label for="date">Date Of Birth</label>
          <div class="reg-form-validator">
            <input type="text" placeholder="dd-mm-yyyy" id="date" name="date" />
            <span class="error-symbol-date" id="error-symbol-date"
              >&#10060;</span
            >
            <span class="done-symbol-date" id="done-symbol-date"
              >&#128504;</span
            >
          </div>

          <label for="email">Email</label>
          <div class="reg-form-validator">
            <input type="email" id="email" name="email"placeholder="example@mail.com"/>
            <span class="error-symbol-email" id="error-symbol-email"
              >&#10060;</span
            >
            <span class="done-symbol-email" id="done-symbol-email"
              >&#128504;</span
            >
          </div>

          <label for="f-or-m-name">Father’s/Mother’s Name</label>
          <div class="reg-form-validator">
            <input type="text" id="f-or-m-name" name="f-or-m-name" />
            <span class="error-symbol-nameFM" id="error-symbol-nameFM"
              >&#10060;</span
            >
            <span class="done-symbol-nameFM" id="done-symbol-nameFM"
              >&#128504;</span
            >
          </div>

          <label for="tel">Mobile No.</label>
          <div class="reg-form-validator">
            <input type="tel" id="tel" name="tel"placeholder="+380XXXXXXXXX"/>
            <span class="error-symbol-tel" id="error-symbol-tel">&#10060;</span>
            <span class="done-symbol-tel" id="done-symbol-tel">&#128504;</span>
          </div>

          <label for="password">Password</label>
          <div class="reg-form-validator">
            <input
              type="password"
              placeholder="min: 8 characters"
              id="password"
              name="password"
            />
            <span class="error-symbol-password" id="error-symbol-password"
              >&#10060;</span
            >
            <span class="done-symbol-password" id="done-symbol-password"
              >&#128504;</span
            >
          </div>

          <label for="confirm-password">Re-enter Password</label>
          <div class="reg-form-validator">
            <input
              type="password"
              id="confirm-password"
              name="confirm-password"
            />
            <span class="error-symbol-repass" id="error-symbol-repass"
              >&#10060;</span
            >
            <span class="done-symbol-repass" id="done-symbol-repass"
              >&#128504;</span
            >
          </div>

          <label for="home-number">home Number</label>
          <div class="reg-form-validator">
            <input type="tel" id="home-number" name="home-number" />
            <span class="error-symbol-home" id="error-symbol-home"
              >&#10060;</span
            >
            <span class="done-symbol-home" id="done-symbol-home"
              >&#128504;</span
            >
          </div>

          <div><button type="submit" id="submit-btn" class="submit-btn">SUBMIT</button></div> `;

form.insertAdjacentHTML('afterbegin',regFormPage);

const nameRegex = /^[a-zA-ZA-Яа-я\s]+$/;
const dobRegex = /^\d{2}\.\d{2}\.\d{4}$/;
const emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
const fatherNameRegex = /^[a-zA-Z\s]+$/;
const mobileNumberRegex = /^\+380\d{9}$/;
const passwordRegex = /^[a-zA-Z\d]{8,}$/;
const rePasswordRegex = /^[a-zA-Z\d]{8,}$/;
const homeRegex = /^(?=.*\d{7,}$).*/;
const firstName = document.getElementById("name");
const date = document.getElementById("date");
const email = document.getElementById("email");
const fomName = document.getElementById("f-or-m-name");
const mobileNumber = document.getElementById("tel");
const password = document.getElementById("password");
const reEnterPass = document.getElementById("confirm-password");
const homeNumber = document.getElementById("home-number");
const submitButton = document.getElementById("submit-btn");

const erorSpanName = document.getElementById("error-symbol-name");
const doneSpanName = document.getElementById("done-symbol-name");
const erorSpanDate = document.getElementById("error-symbol-date");
const doneSpanDate = document.getElementById("done-symbol-date");
const erorSpanEmail = document.getElementById("error-symbol-email");
const doneSpanEmail = document.getElementById("done-symbol-email");
const erorSpanNameFM = document.getElementById("error-symbol-nameFM");
const doneSpanNameFM = document.getElementById("done-symbol-nameFM");
const erorSpanTel = document.getElementById("error-symbol-tel");
const doneSpanTel = document.getElementById("done-symbol-tel");
const erorSpanPass = document.getElementById("error-symbol-password");
const doneSpanPass = document.getElementById("done-symbol-password");
const erorSpanRePass = document.getElementById("error-symbol-repass");
const doneSpanRePass = document.getElementById("done-symbol-repass");
const erorSpanHome = document.getElementById("error-symbol-home");
const doneSpanHome = document.getElementById("done-symbol-home");






function validatingName() {
  if (!nameRegex.test(firstName.value)) {
    erorSpanName.classList.remove("error-symbol-name");
    firstName.style.border = "3px solid red";
    doneSpanName.classList.add("done-symbol-name");
  } else {
    erorSpanName.classList.add("error-symbol-name");
    firstName.style.border = "3px solid green";
    doneSpanName.classList.remove("done-symbol-name");
  }
}
function validatingDate() {
  if (!dobRegex.test(date.value)) {
    erorSpanDate.classList.remove("error-symbol-date");
    date.style.border = "3px solid red";
    doneSpanDate.classList.add("done-symbol-date");
  } else {
    erorSpanDate.classList.add("error-symbol-date");
    date.style.border = "3px solid green";
    doneSpanDate.classList.remove("done-symbol-date");
  }
}
function validatingEmail() {
  if (!emailRegex.test(email.value)) {
    erorSpanEmail.classList.remove("error-symbol-email");
    email.style.border = "3px solid red";
    doneSpanEmail.classList.add("done-symbol-email");
  } else {
    erorSpanEmail.classList.add("error-symbol-email");
    email.style.border = "3px solid green";
    doneSpanEmail.classList.remove("done-symbol-email");
  }
}
function validatingNameFM() {
  if (!fatherNameRegex.test(fomName.value)) {
    erorSpanNameFM.classList.remove("error-symbol-nameFM");
    fomName.style.border = "3px solid red";
    doneSpanNameFM.classList.add("done-symbol-nameFM");
  } else {
    erorSpanNameFM.classList.add("error-symbol-nameFM");
    fomName.style.border = "3px solid green";
    doneSpanNameFM.classList.remove("done-symbol-nameFM");
  }
}
function validatingTel() {
  if (!mobileNumberRegex.test(mobileNumber.value)) {
    erorSpanTel.classList.remove("error-symbol-tel");
    mobileNumber.style.border = "3px solid red";
    doneSpanTel.classList.add("done-symbol-tel");
  } else {
    erorSpanTel.classList.add("error-symbol-tel");
    mobileNumber.style.border = "3px solid green";
    doneSpanTel.classList.remove("done-symbol-tel");
  }
}
function validatingPass() {
  if (!passwordRegex.test(password.value)) {
    erorSpanPass.classList.remove("error-symbol-password");
    password.style.border = "3px solid red";
    doneSpanPass.classList.add("done-symbol-password");
  } else {
    erorSpanPass.classList.add("error-symbol-password");
    password.style.border = "3px solid green";
    doneSpanPass.classList.remove("done-symbol-password");
  }
}
function validatingRePass() {
  if (
    reEnterPass.value !== password.value
  ) {
    erorSpanRePass.classList.remove("error-symbol-repass");
    reEnterPass.style.border = "3px solid red";
    doneSpanRePass.classList.add("done-symbol-repass");
  } else {
    erorSpanRePass.classList.add("error-symbol-repass");
    reEnterPass.style.border = "3px solid green";
    doneSpanRePass.classList.remove("done-symbol-repass");
  }
}



function validatingTelHome() {
  if (!homeRegex.test(homeNumber.value)) {
    erorSpanHome.classList.remove("error-symbol-home");
    homeNumber.style.border = "3px solid red";
    doneSpanHome.classList.add("done-symbol-home");
  } else {
    erorSpanHome.classList.add("error-symbol-home");
    homeNumber.style.border = "3px solid green";
    doneSpanHome.classList.remove("done-symbol-home");
    doneSpanHome.classList.add("active");
  }
}



form.addEventListener("submit", function(e) {
  e.preventDefault()

  if (
    nameRegex.test(firstName.value) &&
    dobRegex.test(date.value) &&
    emailRegex.test(email.value) &&
    passwordRegex.test(password.value) &&
    rePasswordRegex.test(reEnterPass.value) &&
    fatherNameRegex.test(fomName.value)
    ) {
    const formData = new FormData(e.target);
    const formElementsArray = [...formData.entries()];
    console.log(formElementsArray);
    const obj = {};
    formElementsArray.forEach((e) => {
      obj[e[0]] = e[1];
    });
    console.log(obj);
    const usersArray = localStorage.getItem("users");
    if (usersArray) {
      const parsedArray = JSON.parse(usersArray);
      parsedArray.push(obj);
      const dataForLocalStorage = JSON.stringify(parsedArray);
      localStorage.setItem("users", dataForLocalStorage);
    } else {
      const arrayWithData = [obj];
      const dataForLocalStorage = JSON.stringify(arrayWithData);
      localStorage.setItem("users", dataForLocalStorage);
    }
    alert("Thank you for registration");
    window.document.location = "./Login.html";
  } else {
    alert("Check inputs you did somethink wrong");
  }
})

firstName.addEventListener("blur", validatingName);
date.addEventListener("blur", validatingDate);
email.addEventListener("blur", validatingEmail);
fomName.addEventListener("blur", validatingNameFM);
mobileNumber.addEventListener("blur", validatingTel);
password.addEventListener("blur", validatingPass);
reEnterPass.addEventListener("blur", validatingRePass);
homeNumber.addEventListener("blur", validatingTelHome);
