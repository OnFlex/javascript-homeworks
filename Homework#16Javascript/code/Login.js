const wrapper = document.createElement("div");
wrapper.classList.add("wrapper");
document.body.insertAdjacentElement("afterbegin", wrapper);

const imgContainer = document.createElement("div");
imgContainer.classList.add("img-container");
wrapper.appendChild(imgContainer);

const imgLog = document.createElement("img");
imgLog.alt = "img-log";
imgLog.src = "./img/login.png";
imgContainer.appendChild(imgLog);

const formLog = document.createElement("form");
formLog.classList.add("log-form");

const loginPage = `
        <h2 class="log-title">Login</h2>
        <div class="flex-container">
            <label for="email">Email</label>
            <input type="email" id="email" name="email" placeholder="example@mail.com">
            <label for="password">Password</label>
            <input type="password" id="password" name="password" placeholder="Must have at least an 8 characters">
            <div class="link-container">
                <h5 class="link-text">Not a user?</h5>
                <a href="./Reg-form.html">Register now</a>
            </div>
            </div>
            <button class="log-btn">LOGIN</button>`;
wrapper.appendChild(formLog);
formLog.insertAdjacentHTML("afterbegin", loginPage);

const form = document.querySelector(".log-form");
const email = document.getElementById("email");
const password = document.getElementById("password");
const users = JSON.parse(localStorage.getItem("users"));

form.addEventListener("submit", function validatingLog(e) {
  e.preventDefault();

  const users = JSON.parse(localStorage.getItem("users"));

  let userFind = false;

  for (let i = 0; i < users.length; i++) {
    if (
      users[i].email === email.value &&
      users[i].password === password.value
    ) {
      userFind = true;
      break;
    }
  }

  if (userFind) {
    const spinner = `<div id="spinner-container">
    <div class="spinner"></div>`;
    document.body.insertAdjacentHTML("afterbegin",spinner)
    const spinnerContainer = document.getElementById("spinner-container");
    spinnerContainer.style.display = "block";
    setTimeout(() => {
      window.location.href = "dashboard.html";
    }, 5000);
  } else {
    alert("Invalid email or password. Please try again.");
email.style.border = "3px solid red"
password.style.border = "3px solid red"
  }
});
