/*
* У папці calculator дана верстка макета калькулятора. 
Потрібно зробити цей калькулятор робочим.
* При натисканні на клавіші з цифрами - набір введених цифр має бути показаний на табло калькулятора.
* При натисканні на знаки операторів (`*`, `/`, `+`, `-`) на табло нічого не відбувається - програма чекає введення другого числа для виконання операції.
* Якщо користувач ввів одне число, вибрав оператор і ввів друге число, то при натисканні як кнопки `mrc`, так і будь-якого з операторів, в табло повинен з'явитися результат виконання попереднього виразу.
* При натисканні клавіш `M+` або `M-` у лівій частині табло необхідно показати маленьку букву `m` - це означає, що в пам'яті зберігається число. Натискання на MRC покаже число з пам'яті на екрані. Повторне натискання `MRC` має очищати пам'ять.
*/
let a = "";
let b = "";
let sign = "";
let finish = false;
let memory = 0;
let isMemoryIsClear = true
let orangeBtn = document.querySelector(".button.orange");

const digit = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "."];
const action = ["*", "-", "/", "+"];

// display
const out = document.querySelector(".display > input");
// placeholder.style.textAlign = "left"
console.log(out);
const buttons = document.querySelector(".keys");
buttons.addEventListener("click", (event) => {
  if (event.target.nodeName !== "INPUT") return;
  const key = event.target.value;
  // out.value = "";
  if (key === "C") {
    clearAll();
  }
  if (key === "mrc" && isMemoryIsClear === false) {
    out.value = memory;
    isMemoryIsClear = true
    return;
  }

  if (key === "mrc" && isMemoryIsClear === true) {
    out.value = "0";
    memory = 0;
    clearMemoryDisplay();
    console.log(isMemoryIsClear)
  }

  if (key === "m+") { 
  out.value = memory  += a;
  isMemoryIsClear = false
  updateMemoryDisplay();
  }

  if (key === "m-") {
  out.value = memory -= a;
  isMemoryIsClear = false
  updateMemoryDisplay();
  }
  // если нажата клавиша 0-9 или .
  if (digit.includes(key)) {
    if (b === "" && sign === "") {
      a += key;
      out.value = a;
    } else if (a !== "" && b !== "" && finish) {
      b = key;
      finish = false;
      out.value = b;
      orangeBtn.disabled = false;
    } else {
      b += key;
      out.value = b;
      orangeBtn.disabled = false;
    }
    return;
  }

  // если нажата клавиша + - / *
  if (action.includes(key)) {
    sign = key;
    out.value = a;
    return;
  }

  // нажата =
  if (key === "=") {
    if (b === "") b = a;
    switch (sign) {
      case "+":
        a = +a + +b;
        break;
      case "-":
        a = a - b;
        break;
      case "*":
        a = a * b;
        break;
      case "/":
        if (b === "0") {
          out.value = "Ошибка";
          a = "";
          b = "";
          sign = "";
          return;
        }
        a = a / b;
        break;
    }
    finish = true;
    out.value = a;
  }
});
function updateMemoryDisplay() {
 document.getElementById("img").style.display = "block"
}

function clearMemoryDisplay() {
  document.getElementById("img").style.display = "none"
}

function clearAll() {
  a = "";
  b = "";
  sign = "";
  finish = false;
  out.value = 0;
  memory = 0;
}

