// ---- ДЗ ----

// - Написати функцію `filterBy()`, яка прийматиме 2 аргументи. Перший аргумент - масив, який міститиме будь-які дані,
// другий аргумент - тип даних.
// - Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип
// яких був переданий другим аргументом. Тобто якщо передати масив ['hello', 'world', 23, '23', null], і другим аргументом
// передати 'string', то функція поверне масив [23, null].

function filterBy(array) {
   let anotherArray = [];
   for(let i = 0; i<array.length;i++)
   if(typeof array[i] !== "string"){
   anotherArray.push(array[i])
   }
   return anotherArray;
}
console.log(filterBy(['hello', 'world', 23, '23', null,false,true]))


// Задача 2 Переписати гру шибениця з книги на новий синтасис.

const words = ["программа","макака","прекрасный","оладушек"];
let word = words[Math.floor(Math.random() * words.length)];
let answerArray = [];

for (let i = 0; i < word.length; i++) {
  answerArray[i] = "_";
}

let remainingLetters = word.length;

while (remainingLetters > 0) {
  alert(answerArray.join(" "));
  let guess = prompt("Угадайте букву, или нажмите Отмена для выхода из игры.");
  
  if (guess === null) {
    break;
  } else if (guess.length !== 1) {
    alert("Пожалуйста, введите одиночную букву.");
  } else {
    for (let j = 0; j < word.length; j++) {
      if (word[j] === guess) {
        answerArray[j] = guess;
        remainingLetters--;
      }
    }
  }
}

alert(answerArray.join(" "));
alert(`Отлично! Было загадано слово ${word}`);
